#!/usr/bin/make -f

DPKG_EXPORT_BUILDFLAGS := 1
# LTO: https://patches.ubuntu.com/a/ahven/ahven_2.8-9ubuntu3.patch
DEB_BUILD_MAINT_OPTIONS := hardening=+all, optimize=-lto
DEB_LDFLAGS_MAINT_APPEND := -Wl,--no-undefined \
  -Wl,--no-copy-dt-needed-entries -Wl,--no-allow-shlib-undefined
include /usr/share/dpkg/buildflags.mk
include /usr/share/dpkg/buildopts.mk
include $(wildcard /usr/share/ada/packaging.mk)
# wildcard means: not during -indep builds.

conf_args := \
  GNAT_BUILDER=gprbuild \
  GNAT_BUILDER_FLAGS='$(GPRBUILDFLAGS) -Xahven_soversion=$(ahven_SO_VERSION)' \
  includedir=/$(DEB_ADA_SOURCE_DIR) \
  libdir=/$(DEB_LIB_DIR) \
  alidir=/$(DEB_ADA_LIB_INFO_DIR) \
  gprdir=/$(DEB_GNAT_PROJECT_DIR) \
  miscdocdir=/usr/share/doc \
  $(DEB_BUILD_OPTION_PARALLEL:%=SPHINXOPTS=-j%) \
  DESTDIR=$(CURDIR)/debian/tmp \
  # EOL

%:
	dh ${@}

override_dh_auto_configure-arch:
	$(MAKE) configure builddir=../obj-dynamic library_type=dynamic $(conf_args)
	$(MAKE) configure builddir=../obj-static library_type=static $(conf_args)

override_dh_auto_configure-indep:
ifeq (,$(filter nodoc,$(DEB_BUILD_OPTIONS)))
	$(MAKE) configure builddir=../obj-docs $(conf_args)
endif

override_dh_auto_build-arch:
	$(MAKE) -C obj-dynamic
	$(MAKE) -C obj-static

override_dh_auto_build-indep:
ifeq (,$(filter nodoc,$(DEB_BUILD_OPTIONS)))
	$(MAKE) -C obj-docs docs
endif

override_dh_auto_test-arch:
	LD_LIBRARY_PATH=stage/$(DEB_LIB_DIR) $(MAKE) -C obj-dynamic check

override_dh_auto_test-indep:

override_dh_auto_install-arch:
	$(MAKE) -C obj-dynamic install
  # May comfignat install the sources too?
	install -m644 -Dtdebian/tmp/$(DEB_ADA_SOURCE_DIR)/ahven \
	  src/ahven*.ad[bs] src/unix/ahven_compat.ad[bs]
  # May comfignat install the static archive only?
	install -m644 -Dtdebian/tmp/$(DEB_LIB_DIR) \
	  obj-static/stage/$(DEB_LIB_DIR)/libahven.a

override_dh_auto_install-indep:
ifeq (,$(filter nodoc,$(DEB_BUILD_OPTIONS)))
	$(MAKE) -C obj-docs install
  # Rename the User Guide here. dh_installdocs cannot rename, and
  # dh_install would complain with the nodoc profile.
	cd debian/tmp/usr/share/doc/ahven && mv html ug
endif

override_dh_auto_clean:
	rm -fr obj-dynamic obj-static obj-docs
	rm -f gnat_linux/comfignat.gpr
